#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>
#include <iomanip>
#include <algorithm>
#include <cstdint>

// Function to read .bin file into a matrix
/*std::vector<std::vector<uint8_t>> readBinFile(const std::string& filename) {
    std::ifstream file(filename, std::ios::binary);
    std::vector<std::vector<uint8_t>> matrix;

    if (file.is_open()) {
        while (!file.eof()) {
            std::vector<uint8_t> row(8);
            for (int i = 0; i < 8; ++i) {
                char buffer;
                file.read(&buffer, 1);
                if (file.gcount() != 0) {
                    uint8_t num1 = (buffer >> 4) & 0x0F; // first 4 bits
                    i++;
                    uint8_t num2 = buffer & 0x0F; // second 4 bits
                    row[i - 1] = num2;
                    row[i] = num1;
                }
            }
            if (file.gcount() != 0) {
                std::reverse(row.begin(), row.end());
                matrix.push_back(row);
            }
        }
        file.close();
    } else {
        std::cout << "Unable to open file";
    }

    return matrix;
}*/


std::vector<uint8_t> applyXOR(const std::vector<uint8_t>& vector1, const std::vector<uint8_t>& vector2) {
    // Check if vectors are of the same size
    if (vector1.size() != vector2.size()) {
        std::cerr << "Error: Input vectors must be of the same size" << std::endl;
        return {};
    }

    std::vector<uint8_t> result;
    for (std::size_t i = 0; i < vector1.size(); ++i) {
        // Apply XOR operation between corresponding elements of both vectors
        result.push_back(vector1[i] ^ vector2[i]);
    }

    return result;
}


std::vector<uint8_t> addVectors(const std::vector<uint8_t>& vector1, const std::vector<uint8_t>& vector2) {
    // Check if vectors are of the same size
    if (vector1.size() != vector2.size()) {
        std::cerr << "Error: Input vectors must be of the same size" << std::endl;
        return {};
    }

    std::vector<uint8_t> result(8);
    uint8_t carry = 0;
    for (int i = 7; i >= 0; --i) {
        uint16_t sum = vector1[i] + vector2[i] + carry;
        result[i] = sum & 0xF; // keep only the last 4 bits
        carry = (sum >> 4) & 0xF; // keep the carry
    }

    return result;
}


template <typename T>
void printMatrix(const std::vector<std::vector<T>>& matrix) {
    for (const auto& row : matrix) {
        for (const auto& element : row) {
            std::cout << std::hex << std::setw(1) << static_cast<int>(element) << " ";
        }
        std::cout << std::endl;
    }
}


// Function to convert std::vector<uint8_t> to std::string
std::string toBinaryString(const std::vector<uint8_t>& data) {
    std::string result;
    for (uint8_t byte : data) {
        std::bitset<4> bits(byte);
        result += bits.to_string();
    }
    return result;
}

// Function to convert std::string back to std::vector<uint8_t>
std::vector<uint8_t> fromBinaryString(const std::string& str) {
    std::vector<uint8_t> result;
    for (std::size_t i = 0; i < str.size(); i += 4) {
        std::bitset<4> bits(str.substr(i, 4));
        result.push_back(static_cast<uint8_t>(bits.to_ulong()));
    }
    return result;
}

std::vector<uint8_t> rotateBits(const std::vector<uint8_t>& data, int n = 11) {
    // Convert input data to a string of bits
    std::string binary = toBinaryString(data);

    // Add the first n bits to the end of the string
    binary += binary.substr(0, n);

    // Remove the first n bits from the string
    binary = binary.substr(n);

    // Convert the string of bits back to a vector
    std::vector<uint8_t> result = fromBinaryString(binary);

    return result;
}


std::vector<std::vector<uint8_t>> simple_coding(const std::vector<std::vector<uint8_t>>& data,
                                                const std::vector<std::vector<uint8_t>>& table,
                                                const std::vector<std::vector<uint8_t>>& key) {    
    std::vector<std::vector<uint8_t>> result;
    size_t numPairs = data.size()/2;  // Number of 32-bit pairs
    for (size_t i = 0; i < numPairs; ++i) {
        // create containers where calculations should be
        std::pair<std::vector<uint8_t>, std::vector<uint8_t>> N_containers;
        std::pair<std::vector<uint8_t>, std::vector<uint8_t>> sum_containers;
        std::pair<std::vector<uint8_t>, std::vector<uint8_t>> R_containers;

        // fill N containers with data
        size_t currentIndex = i * 2;
        N_containers.first = data.at(currentIndex);
        N_containers.second = data.at(currentIndex+1);

        for (size_t _ = 0; _ < 3; ++_) {
            for (size_t j = 0; j < 8; ++j) {
                // applying xor to numbers from key and container
                sum_containers.first = addVectors(N_containers.first, key.at(j));

                // get indexes from sum_container 1 and get values from matrix
                std::vector<uint8_t> staff(8);
                for (size_t k = 0; k < 8; ++k) {
                    staff.at(k) = table.at(sum_containers.first.at(k)).at(k);
                }
                R_containers.first = staff;

                // slide bits for 11 steps to higher bit
                R_containers.second = rotateBits(R_containers.first);

                // add N2 to R2 by modulus of 2
                sum_containers.second = applyXOR(R_containers.second, N_containers.second);

                // prepare data for next cycles
                N_containers.second = N_containers.first;
                N_containers.first = sum_containers.second;
            }
        }

        for (int j = 7; j >= 0; --j) {
            // applying xor to numbers from key and container
            sum_containers.first = addVectors(N_containers.first, key.at(j));

            // get indexes from sum_container 1 and get values from matrix
            std::vector<uint8_t> staff(8);
            for (size_t k = 0; k < 8; ++k) {
                staff.at(k) = table.at(sum_containers.first.at(k)).at(k);
            }
            R_containers.first = staff;

            // slide bits for 11 steps to higher bit
            R_containers.second = rotateBits(R_containers.first);

            // add N2 to R2 by modulus of 2
            sum_containers.second = applyXOR(R_containers.second, N_containers.second);

            // prepare data for next cycles
            N_containers.second = N_containers.first;
            N_containers.first = sum_containers.second;
        }
        result.push_back(N_containers.second);
        result.push_back(N_containers.first);
    }
    return result;
}



int main() {
    // Example of using read function
    //std::vector<std::vector<uint8_t>> matrix = readBinFile("table.bin");
    //std::vector<std::vector<uint8_t>> key = readBinFile("key.bin");
    std::vector<std::vector<uint8_t>> matrix = {
        {0x2, 0xf, 0x3, 0xf, 0x1, 0xe, 0x5, 0x4},
        {0x3, 0x6, 0x6, 0x5, 0x6, 0x6, 0x7, 0x5},
        {0x8, 0x2, 0x8, 0x3, 0x2, 0x4, 0xd, 0x8},
        {0x3, 0x7, 0x6, 0x7, 0x6, 0x7, 0x6, 0x6},
        {0x1, 0xf, 0x1, 0x9, 0xc, 0x1, 0x5, 0x9},
        {0x3, 0x5, 0x6, 0x6, 0x6, 0x6, 0x6, 0x6},
        {0x4, 0x7, 0xe, 0xd, 0x5, 0x9, 0xe, 0x3},
        {0x3, 0x4, 0x6, 0x6, 0x6, 0x6, 0x6, 0x7},
        {0x7, 0xf, 0x7, 0x0, 0xf, 0xe, 0x4, 0xf},
        {0x3, 0x4, 0x6, 0x7, 0x5, 0x6, 0x7, 0x5},
        {0xd, 0x3, 0x5, 0xc, 0x6, 0x3, 0xf, 0x4},
        {0x2, 0x5, 0x6, 0x6, 0x6, 0x7, 0x5, 0x6},
        {0x8, 0x4, 0xf, 0x5, 0xf, 0xf, 0x3, 0xf},
        {0x3, 0x5, 0x5, 0x6, 0x6, 0x5, 0x6, 0x6},
        {0x9, 0xd, 0x6, 0xf, 0x2, 0x4, 0xf, 0x3},
        {0x3, 0x2, 0x6, 0x5, 0x7, 0x7, 0x6, 0x6},
    };
    std::vector<std::vector<uint8_t>> key = {
        {0x5, 0xf, 0x7, 0x9, 0x6, 0x5, 0x6, 0xb},
        {0x5, 0xf, 0x7, 0x2, 0x6, 0xf, 0x6, 0x6},
        {0x7, 0x0, 0x6, 0xd, 0x6, 0x9, 0x7, 0x3},
        {0x6, 0x1, 0x6, 0x8, 0x6, 0x3, 0x5, 0xf},
        {0x5, 0xf, 0x6, 0x5, 0x6, 0x7, 0x6, 0xe},
        {0x5, 0x4, 0x5, 0x3, 0x4, 0xf, 0x4, 0x7},
        {0x3, 0x4, 0x3, 0x1, 0x3, 0x8, 0x3, 0x2},
        {0x3, 0x9, 0x3, 0x8, 0x2, 0xd, 0x3, 0x7},
    };

    // Get data
    std::string data = "This is some simple text for testing algorithm";

    std::vector<std::vector<uint8_t>> text = {
        {0x7, 0x0, 0x6, 0xd, 0x6, 0x9, 0x5, 0x3},
        {0x7, 0x4, 0x2, 0x0, 0x6, 0x5, 0x6, 0xc},
        {0x2, 0x0, 0x7, 0x4, 0x7, 0x8, 0x6, 0x5},
        {0x7, 0x4, 0x2, 0x0, 0x6, 0xf, 0x7, 0x4},
        {0x2, 0x0, 0x7, 0x4, 0x7, 0x3, 0x6, 0x5},
        {0x6, 0xf, 0x6, 0x7, 0x6, 0xc, 0x6, 0x1},
        {0x6, 0x8, 0x7, 0x4, 0x6, 0x9, 0x7, 0x2},
        {0x2, 0x0, 0x2, 0x0, 0x2, 0x0, 0x6, 0xd},
    };

    // Check if matrix and key are parsed correctly
    std::cout << "Matrix:" << std::endl;
    printMatrix(matrix);

    std::cout << std::endl << "Key:" << std::endl;
    printMatrix(key);

    // Use data in algorighm
    std::vector<std::vector<uint8_t>> coded = simple_coding(text, matrix, key);

    // Print result 
    std::cout << std::endl << "Encoded data is:" << std::endl;
    printMatrix(coded);

    return 0;
}

/*
#include <iostream>
#include <fstream>
#include <vector>
#include <bitset>

// Function to read a binary file into a matrix of boolean values
std::vector<std::vector<bool>> readFromFile(const std::string& filename, int numColumns = 32) {
    std::ifstream file(filename, std::ios::binary);
    if (!file.is_open()) {
        std::cerr << "Error opening file: " << filename << std::endl;
        return {};
    }

    std::vector<std::vector<bool>> matrix;
    std::vector<bool> row;
    char byte;
    int bitsCounter = 0;

    while (file.get(byte)) {
        std::bitset<8> bits(byte);
        for (int i = 7; i >= 0; --i) {
            row.push_back(bits[i]);
            ++bitsCounter;

            if (bitsCounter % numColumns == 0) {
                matrix.push_back(row);
                row.clear();
            }
        }
    }

    file.close();
    return matrix;
}

// Function to perform modulo 2^32 addition between two vectors of bools
std::vector<bool> sumModulo2Power32(const std::vector<bool>& vector1, const std::vector<bool>& vector2) {
    std::uint32_t sum = 0;
    std::uint32_t power = 1;

    // Calculate the sum modulo 2^32
    for (std::size_t i = 0; i < vector1.size(); ++i) {
        if (vector1[i]) {
            sum = (sum + power) % (1UL << 32);
        }
        if (vector2[i]) {
            sum = (sum + power) % (1UL << 32);
        }
        power = (power << 1) % (1UL << 32);
    }

    // Convert the sum back to a vector of bool
    std::vector<bool> result;
    for (int i = 0; i < 32; ++i) {
        result.push_back((sum >> i) & 1);
    }

    return result;
}

// Function to perform simple coding
std::vector<bool> simple_coding(const std::string& data,
    const std::vector<std::vector<bool>>& table,
    const std::vector<std::vector<bool>>& key) {
    // Reading data and converting it to bits
    std::vector<bool> data_bits;
    for (const char& c : data) {
        for (int i = 7; i >= 0; --i) {
            bool bit = (c >> i) & 1;
            data_bits.push_back(bit);
        }
    }
    // Calculate the number of padding bits needed
    int padding_bits = 64 - (data_bits.size() % 64);
    // Add padding bits
    for (int i = 0; i < padding_bits; ++i) {
        data_bits.push_back(false);
    }

    std::vector<std::pair<std::vector<bool>, std::vector<bool>>> result_pairs;
    size_t numPairs = data_bits.size() / 64;  // Number of 32-bit pairs
    for (size_t i = 0; i < numPairs; ++i) {
        // Extract the first 32-bit pair
        std::vector<bool> firstHalf(data_bits.begin() + i * 64, data_bits.begin() + i * 64 + 32);
        // Extract the second 32-bit pair
        std::vector<bool> secondHalf(data_bits.begin() + i * 64 + 32, data_bits.begin() + i * 64 + 64);
        // Create a pair of 32-bit halves and add it to the result vector
        result_pairs.emplace_back(firstHalf, secondHalf);

        // Perform calculations here using key, table, firstHalf, secondHalf
        // For example:
        std::vector<bool> sum_result = sumModulo2Power32(key[0], firstHalf);
        // Process other steps based on your algorithm

        // Store the results in appropriate containers
        // For example:
        // result_pairs.emplace_back(sum_result, other_result);
    }

    return data_bits;
}

// Function to print a matrix
template <typename T>
void printMatrix(const std::vector<std::vector<T>>& matrix) {
    for (const auto& row : matrix) {
        for (const auto& element : row) {
            std::cout << element << " ";
        }
        std::cout << std::endl;
    }
}

int main() {
    // Example of using read function
    std::vector<std::vector<bool>> matrix = readFromFile("table.bin");
    std::vector<std::vector<bool>> key = readFromFile("key.bin");

    // Check if matrix and key are parsed correctly
    std::cout << "Matrix:" << std::endl;
    printMatrix(matrix);

    std::cout << std::endl << "Key:" << std::endl;
    printMatrix(key);

    // Get data
    std::string data = "This is some simple text for testing algorithm";

    // Use data in algorithm
    std::vector<bool> a = simple_coding(data, matrix, key);
    std::cout << a.at(0) << std::endl; // Print example result

    return 0;
}
*/